<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Magazine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MagazineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $magazines=Magazine::all();

        return view('magazine.index', compact('magazines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('magazine.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $magazine = Magazine::create([

            'title'=>$request->input('title'),
            'topic'=>$request->input('topic'),
            'description'=>$request->input('description'),
            'img'=>$request->file('img')->store('public/img'),
            'user_id'=>Auth:: id(),


        ]);

        return redirect(route('magazine.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Magazine  $magazine
     * @return \Illuminate\Http\Response
     */
    public function show(Magazine $magazine)
    {
        return view('magazine.show', compact('magazine'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Magazine  $magazine
     * @return \Illuminate\Http\Response
     */
    public function edit(Magazine $magazine)
    {   
        
        return view('magazine.edit', compact('magazine'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Magazine  $magazine
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Magazine $magazine)
    {
        $magazine->title=$request->title;
        $magazine->topic=$request->topic;
        $magazine->description=$request->description;

        if($request->img) {
            $magazine->img=$request->file('img')->store('public/img');
        }
        $magazine->save();

        return redirect(route('magazine.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Magazine  $magazine
     * @return \Illuminate\Http\Response
     */
    public function destroy(Magazine $magazine)
    {
        $magazine->delete();

        return redirect(route('magazine.index'));
    }

    public function auth($auth){
        $magazines=Magazine::where('user_id', $auth)->get();
        $user=User::find($auth);
        return view('magazine.auth', compact('magazines', 'user'));
    }

}
