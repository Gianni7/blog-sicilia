<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{
    public function  homepage() {
        return view('welcome');
    }

    public function contatti() {
        return view('contatti');
    }

    public function submit(Request $req) {

        $user=$req->input('user');
        $message=$req->input('message');
        $email=$req->input('email');

        $contact = compact('user', 'message', 'email');
        // dd($contact);
        Mail::to($email)->send(new ContactMail($contact));

        return redirect (route('homepage'))->with('message', 'la tua richiesta è stata inoltrata');
    }

    // public function index()
    // {
    //     $articles=Article::all();
        
    //     return view('article.index', compact('articles'));
    // }

    
}
