<x-layout>
    <x-navbar></x-navbar>
    <div class="container">
        <div class="row">
            <h1 class="text-center mt-4">Effettua l'accesso</h1>
            <div class="col-12 col-md-6 offset-md-3">
                <form method="POST" action="{{route('login')}}">
                    @csrf
                    <div class="mb-3">
                        <label for="name1" class="form-label">Nome utente</label>
                        <input type="text" name="name" class="form-control" id="name1">
                      </div>
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Email</label>
                      <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Password</label>
                      <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                    </div> 
                    <button type="submit" class="btn btn-primary">Invia</button>
                  </form>
            </div>
        </div>
    </div>
</x-layout>