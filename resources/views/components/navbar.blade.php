<nav class="navbar navbar-expand-lg navbar-light bg-nav-custom">
  <div class="container-fluid">
    <a class="navbar-brand my-3 fw-bold" href="{{route('homepage')}}">Blog<span id="span-sicilia">Sicilia</span></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav ms-auto my-3">
        @guest
        <li class="nav-item active me-3">
          <a class="nav-link" href="/">Home<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item me-3">
          <a class="nav-link" href="{{route('article.index')}}">Tutti gli Articoli</a>
        </li>
        <li class="nav-item me-3">
          <a class="nav-link" href="{{route('magazine.index')}}">Tutti i Magazine</a>
        </li>
        <li class="nav-item me-3">
          <a class="nav-link" href="{{route('register')}}">Registrati</a>
        </li>
        <li class="nav-item me-3">
          <a class="nav-link" href="{{route('login')}}">Login</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('contatti')}}">Contatti</a>
        </li>
        @else
        <li class="nav-item active me-3">
          <a class="nav-link" href="/">Home<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item me-3">
          <a class="nav-link" href="{{route('article.index')}}">Tutti gli Articoli</a>
        </li>
        <li class="nav-item me-3">
          <a class="nav-link" href="{{route('magazine.index')}}">Tutti i Magazine</a>
        </li>
        <li class="nav-item me-3">
          <a class="nav-link" href="{{route('article.create')}}">Inserisci Articolo</a>
        </li>
        <li class="nav-item me-3">
          <a class="nav-link" href="{{route('magazine.create')}}">Inserisci Magazine</a>
        </li>
        <li class="nav-item me-3">
          <a class="nav-link" href="{{route('contatti')}}">Contatti</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Ciao, {{Auth::user()->name}}
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault();
            document.getElementById('form-logout').submit();">Logout</a>
            <form method="POST" action="{{route('logout')}}" id="form-logout">
              @csrf
            </form>
          </div>
        </li>
        @endguest
      </ul>
    </div>
    </div>
  </nav>