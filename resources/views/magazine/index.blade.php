<x-layout>
    <x-navbar></x-navbar>
    <div class="container">
        <div class="row">
            <h1 class="mt-5 text-center">Elenco delle riviste</h1>
            @foreach ($magazines as $magazine)
            <div class="col-12 col-md-4 mt-5">
                <div class="card shadow">
                    <img src="{{Storage::url($magazine->img)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">{{$magazine->title}}</h5>
                      <h5 class="card-title">{{$magazine->topic}}</h5>
                      {{-- <p class="card-text">{{$magazine->description}}</p> --}}
                      <a href="{{route('magazine.show', compact('magazine'))}}" class="btn btn-primary">Clicca per leggere la rivista</a>
                    </div>
                  </div>
            </div>
            @endforeach
        </div>
    </div>
</x-layout>