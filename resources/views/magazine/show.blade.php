<x-layout>
    <x-navbar></x-navbar>
    <div class="container">
        <div class="row">
            <h1 class="mt-5 text-center">Elenco delle riviste</h1>
            {{-- @foreach ($magazines as $magazine) --}}
            <div class="col-12 col-md-6 offset-md-3 mt-5 mb-5">
                <div class="card shadow">
                    <img src="{{Storage::url($magazine->img)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">{{$magazine->title}}</h5>
                      <h5 class="card-title">{{$magazine->topic}}</h5>
                      <p class="card-text">{{$magazine->description}}</p>
                      <div class="mb-3">
                      <a class="card-text fst-italic" href="{{route('magazine.auth', ['auth'=>$magazine->user->id])}}">{{$magazine->user->name}}</a>
                      </div>
                      <a href="{{route('magazine.index')}}" class="btn btn-primary">Torna indietro</a>
                      @if ($magazine->user->id == Auth::id())  
                      <a href="{{route('magazine.edit', compact('magazine'))}}" class="btn btn-primary">Modifica</a>
                      <form method="POST" action="{{route('magazine.destroy', compact('magazine'))}}">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger mt-3">Elimina la rivista</button>
                      </form>
                      @endif

                    </div>
                  </div>
            </div>
            {{-- @endforeach --}}
        </div>
    </div>
</x-layout>

