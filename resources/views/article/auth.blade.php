<x-layout>
    <x-navbar></x-navbar>
    <div class="container">
        <div class="row">
            <h1 class="mt-5 text-center">Elenco degli articoli di {{$user->name}}</h1>
            @foreach ($articles as $article)
            <div class="col-12 col-md-6 offset-md-3">
                <div class="card mt-5 mb-5 shadow">
                    <img src="{{Storage::url($article->img)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">{{$article->title}}</h5>
                      <p class="card-text">{{$article->description}}</p>
                      <a href="{{route('article.auth', ['auth'=>$article->user->id])}}" class="card-text">{{$article->user->name}}</a>
                      <a href="{{route('article.index')}}" class="btn btn-primary">Torna indietro</a>
                    </div>
                  </div>
            </div>
            @endforeach
        </div>
    </div>
</x-layout>