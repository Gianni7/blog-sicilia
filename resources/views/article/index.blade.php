<x-layout>
    <x-navbar></x-navbar>
    <div class="container">
        <div class="row">
            <h1 class="mt-5 text-center">Elenco degli articoli</h1>
            @foreach ($articles as $article)
            <div class="col-12 col-md-4 mt-5">
                <div class="card shadow">
                    <img src="{{Storage::url($article->img)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">{{$article->title}}</h5>
                      {{-- <p class="card-text">{{$article->description}}</p> --}}
                      <a href="{{route('article.show', compact('article'))}}" class="btn btn-primary">Clicca per leggere tutto l'articolo</a>
                    </div>
                  </div>
            </div>
            @endforeach
        </div>
    </div>
</x-layout>