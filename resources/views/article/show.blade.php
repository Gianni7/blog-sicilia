<x-layout>
    <x-navbar></x-navbar>
    <div class="container">
        <div class="row">
            <h1 class="mt-5 text-center">Dettaglio articolo</h1>
            {{-- @foreach ($articles as $article) --}}
            <div class="col-12 col-md-6 offset-md-3">
                <div class="card shadow mt-5 mb-5">
                    <img src="{{Storage::url($article->img)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">{{$article->title}}</h5>
                      <p class="card-text">{{$article->description}}</p>
                      <div class="mb-3">
                      <a class="fst-italic" href="{{route('article.auth', ['auth'=>$article->user->id])}}" class="card-text">{{$article->user->name}}</a>
                        </div>
                      <a href="{{route('article.index')}}" class="btn btn-primary">Torna indietro</a>
                      @if ($article->user->id == Auth::id())
                      <a href="{{route('article.edit', compact('article'))}}" class="btn btn-primary ms-2">Modifica l'articolo</a>
                      <form method="POST" action="{{route('article.destroy', compact('article'))}}" class="mt-2">
                          @csrf
                          @method('delete')
                          <button type="submit" class="btn btn-danger">Elimina l'articolo</button>
                      </form>
                      @endif
                    </div>
                  </div>
            </div>
            {{-- @endforeach --}}
        </div>
    </div>
</x-layout>