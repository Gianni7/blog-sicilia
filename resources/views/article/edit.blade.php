<x-layout>
    <x-navbar></x-navbar>
    <form method="POST" action="{{route('article.update', compact('article'))}}" enctype="multipart/form-data">
        @csrf
        <div class="container">
            <div class="row">
                <h1 class="mt-5 text-center">Modifica l'articolo</h1>
                <div class="col-12 col-md-6 offset-md-3">
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Titolo</label>
                        <input type="text" name="title" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$article->title}}">
                      </div>
                      <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Descrizione</label>
                        <textarea class="form-control" name="description" id="exampleInputPassword1" cols="30" rows="10">"{{$article->description}}"</textarea>
                      </div>
                      <div class="mb-3">
                        <img src="{{Storage::url($article->img)}}" alt="" class="img-fluid">
                        <label for="exampleInputPassword1" class="form-label">Inserisci immagine</label>
                        <div>
                        <input type="file" for="exampleInputPassword1" name="img">
                        </div>
                      <button type="submit" class="btn btn-primary mt-4">Modifica</button>
                    </form>
                </div>
            </div>
        </div>
</x-layout>