<x-layout>
    
        <x-navbar></x-navbar>
    
    @if (session('message'))
    <div class="alert alert-success">
        {{ session('message')}}
    </div>
    @endif
    <header class="masthead">
        <div class="container h-100">
          <div class="row h-100 align-items-center">
            <div class="col-12 text-center">
              {{-- <h1 class="font-weight-light">Vertically Centered Masthead Content</h1>
              <p class="lead">A great starter layout for a landing page</p> --}}
            </div>
          </div>
        </div>
      </header>
</x-layout>