<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\MagazineController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'homepage'])->name('homepage');
Route::get('/contatti', [PublicController::class, 'contatti'])->name('contatti');
Route::post('/contatti/submit', [PublicController::class, 'submit'])->name('contatti.submit');
Route::get('/article/create', [ArticleController::class, 'create'])->name('article.create');
Route::post('/article/store', [ArticleController::class, 'store'])->name('article.store');
Route::get('/article/index', [ArticleController::class, 'index'])->name('article.index');
Route::get('/article/show/{article}', [ArticleController::class, 'show'])->name('article.show');
Route::get('/article/edit/{article}', [ArticleController::class, 'edit'])->name('article.edit');
Route::post('/article/update/{article}', [ArticleController::class, 'update'])->name('article.update');
Route::delete('/article/destroy/{article}', [ArticleController::class, 'destroy'])->name('article.destroy');
Route::get('/magazine/create', [MagazineController::class, 'create'])->name('magazine.create');
Route::post('/magazine/store', [MagazineController::class, 'store'])->name('magazine.store');
Route::get('/magazine/index', [MagazineController::class, 'index'])->name('magazine.index');
Route::get('/magazine/show/{magazine}', [MagazineController::class, 'show'])->name('magazine.show');
Route::get('/magazine/edit/{magazine}', [MagazineController::class, 'edit'])->name('magazine.edit');
Route::put('/magazine/update/{magazine}', [MagazineController::class, 'update'])->name('magazine.update');
Route::delete('/magazine/destroy/{magazine}', [MagazineController::class, 'destroy'])->name('magazine.destroy');
Route::get('/magazine/auth/{auth}', [MagazineController::class, 'auth'])->name('magazine.auth');
Route::get('/article/auth/{auth}', [ArticleController::class, 'auth'])->name('article.auth');

